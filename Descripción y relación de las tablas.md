 El punto central de la información son lxs candidatxs, individuos, cada uno de ellos cuenta con una clave única basada en su nombre completo; dado que es la única información coincidente en la publicación de candidaturas del instituto federal y los locales. 

## Candidatos

- `Candidato` Clave única de individuo
- `Nombre` Nombre del candidatx
- `descripción` Una descripción realizada de manera manual, basada en la información recopilada.

## Candidaturas

- `tipo` Tipo de candidatura
- `demarcacion` Espacio geográfico al que está relacionada la candidatura.
- `lista` Número de lista de la candidatura, en dado caso que aplique.
- `partido` Partido que presenta la candidatura
- `candidato` clave única del candidatx
- `etiqueta` aplicable a Síndicos y Fórmulas indígenas

No se incluyen candidatxs suplentes.

Las demarcaciones, se encuentran en función del tipo de candidatura, en los casos en que aplique incluye la clave de la entidad, p.ej. distritos locales y municipios. 

|tipo| demarcación|
|---|---|
|dfed_p| Circunscripción federal|
|dfed| distrito federal|
|gob| Entidad|
|dloc_p| Lista estatal (Qro)|
|reg_p|Municipio|
|reg| Municipio|
|pmun| Municipio|
|dloc| Distrito local|

## partidos

Simplemente las etiquetas de los partidos

## url

- `url` la url
- `Candidato` clave de candidatx
- `url_type` emulado de SocialTIC, pero no hace distinción entre página de FB y sitio personal; se agrega un tipo para wikipedia y otro para teléfono.
- `owner_type` mismo que SocialTIC
- `in_api` indica si este url se encuentra en la api electoral

## apielectoral_rel

Simplemente es la tabla de personas de la api electoral, pero incluye solo las candidaturas (contest) coincidentes con la CandiDATXS, y se  agrega el id propio, así como si el candidatx también se encuentra en  nuestra BD; es util solo para relacionar esta información con la de la API electoral. 




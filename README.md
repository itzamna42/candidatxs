# CandiDATXS Data 

Este repositorio simplemente incluye la información, en formato abierto, que se presenta en la aplicación web [*Conoce tus CandiDATXS*](https://candidatxs.com).

Esta aplicación fue desarrollada conjuntamente por [Itzamná](https://itzamná.org), el [LUSC](https://www.facebook.com/search/top?q=laboratorio%20universitario%20de%20seguridad%20ciudadana%20-%20lusc), y la CANACO Querétaro. 

![logos](https://itzamna.org/monstro/post/2021/05/candidatxs/logos.png)

Decidimos hacer pública la información en este momento, en buena medida en respuesta al trabajo que ha realizado [SocialTIC](https://socialtic.org/) con su [API Candidaturas MX](https://socialtic.org/blog/api-candidaturas-mx/); haciendo disponible para quien la desee utilizar. 

El formato de datos que se incluye trató de aproximarse a lo que ofrece SocialTIC, pero dado que ambos proyectos fueron concebidos independientemente, y con finalidades un poco distintas, existen diferencias entre los formatos y estructuras de datos.

Visita https://candidatxs.com para ver como utilizamos estos datos.

Contiene información de *11209 candidatos* en *12670 candidaturas* tanto federales como locales. 

### Commits

- El comit *Post elección* es la ultima actualización de información nueva; a partir de este punto no hay nueva inforamción.  También se incluye una nueva carpeta que incluye los archivos con los datos que se tomaron de la API candidaturas.

- El 1 de Junio se incorporan todas las candidaturas para **San Luis Potosí**,  tanto federales como locales.  De esta manera están ahora las candidaturas para Querétaro y Guanajuato y San Luis Potosí. 

- El 28 de Mayo se incorporan todas las candidaturas para **Guanajuato**, tanto federales como locales.  De esta manera están ahora las candidaturas para Querétaro y Guanajuato. 

- A partir del commit del 20 de mayo, se incluye ya la información que se incorporó a CandiDATXS desde la API Candidaturas MX, no se muestra distinción del origen de los datos.

- El primer commit incluye información de todas las candidaturas y todos lxs candidatxs de las elecciones federales y locales en **Querétaro**.

  

